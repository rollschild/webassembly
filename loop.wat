(module
  (import "env" "log" (func $log (param i32 i32)))  

  (func $loop_test (export "loop_test") (param $n i32)
    (result i32)
    
    (local $i i32) ;; what's the default value of $i?
    ;; default is 0: https://webassembly.github.io/spec/core/exec/runtime.html#values

    (local $factorial i32)    

    (local.set $factorial (i32.const 1))

    (loop $continue (block $break
                           
      ;; i++
      (local.set $i 
        (i32.add (local.get $i) (i32.const 1))
      )                     

      ;; $factorial = $i * $factorial
      (local.set $factorial
        (i32.mul (local.get $i) (local.get $factorial)) 
      )

      (call $log (local.get $i) (local.get $factorial))

      (br_if $break
        ;; if $n === $i, break from loop 
        (i32.eq (local.get $i) (local.get $n))       
      )

      br $continue
    ))

    ;; return $factorial to the calling environment
    local.get $factorial
  )
)

