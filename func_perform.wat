(module
  (import "js" "external_call" (func $external_call (result i32)))

  (global $i (mut i32) (i32.const 0)) 
  ;; global for internal function  
  ;; mutable
  ;; initial value 0
  

  (func $internal_call (result i32)
    ;; $i += 1
    global.get $i
    i32.const 1
    i32.add
    global.set $i
        
    ;; returned to calling function
    global.get $i
  )

  ;; function exported to JS
  (func (export "wasm_call")
    (loop $again
      call $internal_call
      i32.const 4_000_000
      i32.le_u
      br_if $again      
    )      
  )

  (func (export "js_call")
    (loop $again
      (call $external_call)
      i32.const 4_000_000
      i32.le_u
      br_if $again
    )    
  )
)
