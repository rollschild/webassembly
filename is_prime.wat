(module
  (func (export "is_prime") (param $n i32) (result i32)
    (local $i i32)
    
    (if (i32.eq (local.get $n) (i32.const 1))
      (then
        i32.const 0 ;; 1 is not prime
        return  
      )  
    )    

    (if (call $eq_2 (local.get $n))
      (then
        i32.const 1 ;; 2 is prime
        return  
      )  
    )    

    (block $not_prime
      (call $check_even (local.get $n))
      br_if $not_prime ;; not prime      
      
      (local.set $i (i32.const 1))

      (loop $prime_test_loop
        (local.tee $i (i32.add (local.get $i) (i32.const 2))) ;; $i += 2
        local.get $n ;; stack = [$n, $i]

        i32.ge_u ;; $i >= $n
        if
          i32.const 1
          return
        end      

        (call $check_multiple (local.get $n) (local.get $i))
        br_if $not_prime
        br $prime_test_loop
      )
    )

    i32.const 0 ;; return false
  )  
  ;; the function expects a 32-bit integer on the stack when it completes

  (func $check_even (param $n i32) (result i32)
    local.get $n
    i32.const 2
    i32.rem_u
    i32.const 0
    i32.eq ;; returns 1 if n == 0  
  )

  (func $eq_2 (param $n i32) (result i32)
    local.get $n
    i32.const 2
    i32.eq ;; return 1 if n == 2  
  )

  (func $check_multiple (param $n i32) (param $m i32) (result i32)
    local.get $n
    local.get $m
    i32.rem_u ;; $n % $m
    i32.const 0
    i32.eq ;; returns 1 if $n % $m == 0 
  )
)
