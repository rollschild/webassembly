# WebAssembly Text Basics

- every WAT app must be a **module**

```
(module
  ;; module content
  (;
    block comment
  ;)
)
```

- WAT doesn't have native string support
  - working with strings requires working directly with memory as an array of
    character data
  - the memory data must be converted into a string in JS
  - need to declare an array of character data stored within WebAssembly linear
    memory
- need to call an imported JS function from WebAssembly to handle I/O
  - in a WebAssembly module, I/O is handled by the embedding environment
- a **page** is the smallest chunk of memory you can allocate at one time to
  linear memory
  - in WebAssembly a page is 64KB
- if writing to a memory location that is greater than allocated, you will get
  memory error in JS console (???)

## WAT Variables

- the browser manages local/global WAT variables in the same way as it does for
  JS variables
- 4 global/local variable types:
  - `i32`
  - `i64`
  - `f32`
  - `f64`
- strings & other data structures handled directly in linear memory

### Global Variables and Type Conversion

- globals in WAT can be accessed from any function
- globals generally used as _constants_
  - because mutable ones can introduce side effects
- numbers in JS are 64-bit floating-point variables
- the BigInt WebAssembly proposal
  - support for JS BigInt types available in WebAssembly
  - exchange of 64-bit integers between JS and WebAssembly
  - as of 10/02/2021, it's already merged
- in JS, all numbers are treated as 64-bit floating
  - when calling a JS function in WebAssembly,
  - the JS engine implicitly performs a conversion to a 64-bit float
  - no matter what data type
  - but WebAssembly _will_ define the imported function as having a specific
    data type requirement
- you **CANNOT** pass 64-bit integers to JS
  - either convert it to 64-bit float or
  - pass them as two 32-bit integers
- a 32-bit floating point in JS and WebAssembly
  - uses 23 bits to represent the number
  - then multiplies it by two raised to an 8-bit exponent value
- **all** floating point numbers are approximations
- using 32-bit floating _might_ increase performance
- +, -, and \* typically faster with integers
  - / by powers of 2 is also faster
- / by anything but a power of 2 can be faster with floating-point numbers

### Local Variables

- local variables & parameters are pushed onto the stack with `local.get`

### Unpacking S-Expressions

- the browser debugger unpacks the S-Expressions
- the unpacking process evaluates expressions:
  - inside out first
  - then in order

### Indexed Variables

- WAT does **NOT** require you to name variables and functions
- functions and variables can be named after indexed numbers
- good part of this approach is you can declare multiple parameters in a single
  expression by adding more types

### Converting between types

- `trunc`: truncates the fractional portion of a floating-point when it
  converted to an integer
- `wrap` puts the lower 32 bits of a 64-bit integer into a `i32`
- `reinterpret` keeps the bits of an integer/floating-point the same when
  reintereinterpreted as a different data type

## if/else Conditional Logic

- WebAssembly does not have a boolean type
  - instead, it uses i32
- an `if` statement requires an `i32` to be on top of the stack to evaluate
  - any non-zero as true and zero as false

```wat
(if (local.get $bool_i32)
  (then
    nop
  )
  (else
    nop
  )
)
```

- unpacked:

```wat
local.get $bool_i32

if
  nop
else
  nop
end
```

- no `then` in the unpacked version
- `then` in S-Expressions is purely syntactic sugar

```wat
;; if (x > y && y < 6)
(if
  (i32.and
    (i32.gt_s (local.get $x) (local.get $y))
    (i32.lt_s (local.get $y) (i32.const 6))
  )
  (then
    nop
  )
)
```

- `i32.and` - bitwise AND
- binary AND vs. logical AND
- `f32.gt` and `f32.lt`
- many integer operations _must_ specify whether they support negative numbers
  using the `_s` suffix
- **ALL** floating numbers are signed and have a dedicated sign bit

## Loops and Blocks

- if you want the code to jump _backward_,
  - you must put it inside a **loop**
- if you want the code to jump forward,
  - you must put it in a block

### block

- the `block` expression
- the code can only jump to the _end_ of a `block` if it's inside the block
- if the code jumps to the end of a `block`, then it exists inside the `block`

```wat
(block $jump_to_end
  br $jump_to_end

  nop
)

;; br jumps to here
nop
```

- `br` is a branch statement that instructs the program to jump to a different
  location
- if `br` within a block is instructed to jump to the block's label
  - it exits the block and begins to execute code immediately outside the block
- the `br_if` conditional branch

## Loop

- `loop` - jumps to the beginning of a block of code

```wat
(loop $not_gonna_loop
  nop
)

;; the above code exits the loop block at the end
nop
```

- a `loop` expression in WAT does **not** loop on its own
  - it needs a branch statement _inside_ the loop
  - ...to branch back to the beginning of the `loop`
- when executed in the browser, **NEVER** use an infinite loop
  - you need to give control back to browser

### Using block and loop together

- `continue` and `break`

### Branching with br_table

- use `block` with `br_table`
  - similar to `switch` statement
  - when there's a large number of branches
  - the code can only break out of a block it's inside
    - you must declare **ALL** of the blocks ahead of time

```wat
(block $block_0
(block $block_1
(block $block_2

(br_table $block_0 $block_1 $block_2
  (local.get$val)
)

)

;; block 2
i32.const 2
return
)

;; block 1
i32.const 1
return
)

;; block 0
i32.const 0
return
```

-
