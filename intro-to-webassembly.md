# Intro to WebAssembly

## What Is WebAssembly

- **WAT**: WebAssembly's textual representation
  - WebAssembly Text
- WebAssembly is a virtual **Instruction Set Architecture** (ISA) for a stack
  machine
- generally, an ISA is a binary format designed to execute on a specific
  machine
- but WebAssembly is designed to run on a virtual machine
- WebAssembly is a binary target, **NOT** a programming language
- performance improvements in two areas:
  - startup speed
    - webassembly still needs to be parsed but it's faster since it's bytecode
  - throughput

## Reasons to Use WebAssembly

- libraries to port existing libraries to WebAssembly:
  - **wasm-pack** for Rust
  - **Emscripten** for C/C++

## Portability and Security

- a sandboxed environment to run anywhere
- server-side WASI code

## WAT

- writing WASM programs in a language like Rust uses a toolchain, which
  compiles a WebAssembly binary file as well as JS glue code and HTML that
  embeds the WebAssembly module
- two coding styles:
  - the **linear instruction list** style
    - you need to mentally keep track of items on the stack
  - **S-Expressions**
    - tree-like
- webassembly is compiled into a bytecode _ahead_ of time
- JS must be parsed and tokenized before the JIT compiler can turn it into
  bytecode

## Stack Machines

- WebAssembly is a virtual stack machine
- LIFO
- ISA for stack machine vs. ISA for register machine
- advantage of stack machine:
  - smaller bytecode sizes

## S-Expressions

- when writing a WAT function, we enclose the function in parentheses
- just syntactic sugar

## The Embedding Environment

- environments include WASI
- the embedding environment manages the stack using the hardware registers
- browser
- WASI
  - a standard for WebAssembly interaction with the OS
  - allows WebAssembly to use the filesystem, make system calls, ad handle I/O
  - **wasmtime** from Mozilla
  - Node.js can also run a WASI experimental preview using the
    `--experimental-wasi-unstable-preview1` flag
- VSCode
- Node.js
- **wat-wasm**

- WebAssembly supports four data types:
  - `i32`
  - `i64`
  - `f32`
  - `f64`
