(module
  (import "env" "print_string" (func $print_string(param i32)))
  (import "env" "buffer" (memory 1)) ;; a single page of linear memory

  ;; a number imported form the embedding environment
  ;; this value will be the starting memory location of the string
  ;; and can be any location in the linear memory page up to the max 65535
  (global $start_string (import "env" "start_string") i32)

  (global $string_len i32 (i32.const 12))

  ;; define the string in the linear memory using a data expression
  ;; first pass the location in memory where the module will write data
  (data (global.get $start_string) "hello world!")

  (func (export "helloworld")
    (call $print_string (global.get $string_len))
  )
)
