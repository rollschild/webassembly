# Functions and Tables

## When to call functions from WAT

- Not every function should use `export`
- Every call to a WebAssembly function from JS incurs an overhead cost
- small functions that don't use many computing cycles might be better kept in
  JS to reduce overhead
- the WAT functions that are suited for exporting are those that loop over and
  process a lot of data

## is_prime

- function purely for export:

```wat
(func (export "is_prime") (param $n i32) (result i32))
```

- function for internal use:

```wst
(func $is_prime (export "is_prime") (param $n i32) (result i32))
```

- Truthiness
  - WebAssembly currently does **NOT** have boolean
  - good practice to use `!!` to force the value `0` to a boolean `true` and any other
    number to a boolean `false`
- `i32.rem_u`: remainder operation
- `local.tee` vs. `local.set`
  - they both set the value of the variable paseed to the value on top of the
    stack
  - `local.set` pops the value **off** the stack
  - `local.tee` leaves the value on the stack
- You do **NOT** have access to data on the stack put there by the calling
  function
  - you must pass in variables as parameters

## Declaring an imported function

```wat
(import "env" "print_string" (func $print_string(param i32)))
```

- `import`: import an object called `print_string` passed inside an object
  called `env`
- when creating a JS callback function, the only data type that function can
  receive is a JS number
- only numbers can be passed as parameters to JS functions
- WebAssembly can pass three of the four main data types back to functions
  imported from JS
  - i32
  - f32
  - f64
- BigInt: you must choose the data type you want to convert it to and perform
  the conversion inside WebAssembly
- WAT does **NOT** support OOP

## Function tables

- WebAssembly has **tables**, which currently can _only_ hold functions
  - function tables if I may?
- allows WebAssembly to dynamically swap functions at runtime
  - which allows compilers to support features such as function pointers and
    OOP virtual functions
- currently only supporting the `anyfunc` type
  - a generic WebAssembly function type
- may support JS objects and DOM elements in the future
- JS and WebAssembly can dynamically change tables at runtime
- There's a performance cost to calling a function from a table rather than
  through an import
  - a function table entry must be called indirectly
- **cannot** add a JS function to a function table from JS side
  - there's `WebAssembly.Table` function set
  - which allows you to set functions in a table, only with a function defined
    in a WebAssembly module
- use `elem` to set the values in the table
  - first parameter: the index of the first element we set
